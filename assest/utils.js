let parseQuery2Json = (query) => {
  let json = {}
  query.split('&').forEach((v) => {
    let pair = v.split("=")
    json[pair[0]] = pair[1]
  })
  
  return json
}

let parseJson2Query = (json) => {
  let query = []
  Object.keys(json).forEach((v) => {
    query.push(`&${v}=`+json[v])
  })
  
  let result = query.join("")
  return result.substr(1, result.length)
}

export { parseQuery2Json, parseJson2Query}