const base = 'http://sey5i7.natappfree.cc'

/**
 * student_id: 学号
 * password: 密码
 * callback: 回调函数
 */
let login = (payload, callback) => {
  wx.request({
    url: `${base}/api/login`,
    header: {
      'content-type': 'application/x-www-form-urlencoded'
    },
    data: payload,
    method: 'POST',
    success: (res) => {
      callback(res)
    },
    fail: ({statusCode}) => {
      wx.showToast({
        title: '网络阻塞,请稍后再试!',
        image: '/assest/img/error.png',
        duration: 2500,
        mask: true
      })
    }
  })
}

/**
 * admission: 准考证号
 * name: 姓名
 * stroe: 回调函数
 */
let getCet = ({admission, name}, store) => {
  wx.showLoading({
    title: '正在查询,请稍等...',
    mask: true
  })
  wx.request({
    url: `${base}/api/cet`,
    method: 'GET',
    header: {
      'content-type': 'application/json'
    },
    data: {
      'zkzh': admission,
      'xm': name
    },
    success: (res) =>{
      typeof store === 'function' && store(res.data)
    },
    complete: (res) => {
      console.log(res)
      wx.hideLoading()
    },
    fail: (res) => {
      wx.showToast({
        image: '/assest/img/error.png',
        duration: 2500,
        title: '网络异常,请检查你的网络..',
        icon: 'success'
      })
    }
  })
}

/**
 * 得到工作室列表
 */
let studioList = (callback) => {
  wx.showLoading({
    title: '正在加载...',
  })
  wx.request({
    url: `${base}/api/studio/0`,
    method: 'GET',
    success: (res) => {
      wx.hideLoading()
      typeof callback == 'function' && callback(res)
    },
    fail: (res) => {
      wx.showToast({
        image: '/assest/img/error.png',
        duration: 2500,
        title: '啊偶,你的网络出问题了',
        icon: 'success'
      })
    }
  })
}

/**
 * 得到工作室详细信息
 * step: 上或下半部
 * status: 请求参数
 */
let studioDetail = ({step, status}, callback) => {
  wx.showLoading({
    title: '正在加载...',
  })
  wx.request({
    url: `${base}/api/studio/${step}`,
    method: 'GET',
    data: {
      status: status
    },
    header: {
      'content-type': 'application/x-www-form-urlencoded'
    },
    success: (res) => {
      wx.hideLoading()
      callback(res)
    },
    fail: (res) => {
      wx.showToast({
        image: '/assest/img/error.png',
        duration: 2500,
        title: '啊偶,你的网络出问题了',
        icon: 'success'
      })
    }
  })
}

/**
 * payload:表单数据
 * filePath: 文件路径
 * callback: 回调函数
 */
let uploadCase = (payload, filePath, callback) => {
  const url = `${base}/api/leave`
  if(filePath)
    wx.uploadFile({
      url: url,
      filePath: filePath,
      name: 'illness',
      formData: payload,
      success: (res) => {
        typeof callback === 'function' && callback(res)
      },
      complete: (res) => {
        wx.hideLoading()
      },
      fail: (res) => {
        wx.showToast({
          title: res.statusCode + ': 网络异常..',
          icon: 'success'
        })
      }
    })
  else 
    wx.request({
      url: url,
      header:{
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: payload,
      success: (res) => {
        typeof callback === 'function' && callback(res)
      }
    })
}

/**
 * infoId: 查询id
 * role: 权限
 * callback: 回调函数
 */
let reviewLeave = ({infoId, role}, callback) => {
  wx.request({
    url: `${base}/api/review/${role}`,
    header: {
      'content-type': 'application/x-www-form-urlencoded'
    },
    method: 'GET',
    data: {
      'info_id': infoId
    },
    success: (res) => {
      typeof callback === 'function' && callback(res)
    },
    fail: (res) => {
      wx.showToast({
        image: '/assest/img/error.png',
        duration: 2500,
        title: '啊偶,你的网络出问题了',
        icon: 'success'
      })
    }
  })
}

/**
 * payload:表单数据
 * callback: 回调函数
 */
let teacherReview = (payload, callback) => {
  wx.request({
    url: `${base}/api/review/teacher`,
    method: 'POST',
    data: payload,
    success: (res) => {
      typeof callback === 'function' && callback(res)
    },
    fail: (res) => {
      wx.showToast({
        image: '/assest/img/error.png',
        duration: 2500,
        title: '啊偶,你的网络出问题了',
        icon: 'success'
      })
    }
  })
}

/**
 * payload: 传递的参数
 *          id email days
 * callback: 回调函数
 */
let download = (payload, callback) => {
 console.log(payload)
  wx.request({
    url: `${base}/api/doc`,
    method:'POST',
    data: payload,
    success: (res) => {
      callback(res)
    },
    fail: (res) => {
      wx.showToast({
        image: '/assest/img/error.png',
        duration: 2500,
        title: '啊偶,你的网络出问题了',
        icon: 'success'
      })
    }
  })
}

export {login, getCet, download,
studioList, studioDetail, uploadCase, reviewLeave, teacherReview}