// cet.js
import { getCet, base } from '../../assest/api.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    result: {}
  },

  submit: function ({detail}) {
    let payload = detail.value

    if (payload.admission.length != 15){
     wx.showModal({
       title: '错误',
       content: '请输入15位准考证号!',
       showCancel: false
     })
    } 
    else if (!payload.name.match(/[^\x00-\xff]/)){
      wx.showModal({
       title: '错误',
       content: '姓名输入有误',
       showCancel: false
     })
    } else {
      getCet(payload, (res) => {
        this.setData({ result: res })
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})