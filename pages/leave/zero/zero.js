// zero.js
import { reviewLeave } from '../../../assest/api.js'
import { parseJson2Query } from '../../../assest/utils.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    text: '',
    zero: '',
    disabled: false,
    banner: 'two'
  },
  seeHistory: function ({currentTarget}) {
    const index = currentTarget.dataset.index
    const query = parseJson2Query(this.data.zero[index])
    wx.navigateTo({
      url: `../leave?${query}&banner=${this.data.banner}`,
    })
  },
  gotoLeave: function (){
    wx.navigateTo({
      url: '../leave',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.showLoading({
      title: '请稍等',
      mask: true
    })
    const user = wx.getStorageSync('user')
    const _this = this

    reviewLeave(user, ({data}) => {
      let text = []
      Object.values(data).forEach(v => {
        let tip = {}
        if (v.t_version === 0) {
          tip = { color: 'orange', text: '班主任正在审核' }
          _this.setData({ disabled: true })
        } else if (v.t_version === 1) {
          if (v.l_version === 0) {
            tip = { color: 'orange', text: '院系正在审核' }
            _this.setData({ disabled: true })
          } else if (v.l_version === 1) {
            tip = { color: 'green', text: '请假申请通过' }
          } else if (v.l_version === 2) {
            tip = { color: 'red', text: '院系驳回了请求' }
          }
          _this.setData({ banner: 'three' })
        } else if (v.t_version === 2) {
          tip = { color: 'red', text: '班主任驳回了请求' }
        }

        text.push(tip)
      })

      _this.setData({ text: text })
      _this.setData({ zero: data })
      wx.hideLoading()
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})