// detail.js
import { teacherReview } from '../../../assest/api.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: '',
    detail: '',//上一页面传递的参数
    hiddenInput: true//隐藏拒绝理由输入框
  },

  seeCase: function(e){
    const url = this.data.detail.url
    wx.previewImage({
      urls: [url]
    })
  },

  changeRadio: function({detail}){
    if (Number(detail.value) === 1){
      this.setData({ hiddenInput: true})
    } else {
      this.setData({ hiddenInput: false})
    }
  },
  submit: function ({detail}){
    let payload = detail.value
    payload.id = this.data.detail.id
    payload.name = this.data.detail.name
    payload.clazz = this.data.detail.clazz
    payload.days = this.data.detail.days
    wx.showLoading({
      title: '请稍等..',
    })
    teacherReview(payload, ({data}) => {
      if(data){
       wx.hideLoading()
       wx.navigateBack({
         delta: 1,
       })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const user = wx.getStorageSync('user')
    this.setData({ user: user })

    let start = options.from.replace(/[^0-9]/ig, ",").split(",")
    let end = options.end.replace(/[^0-9]/ig, ",").split(",")
    options.days = (end[0] - start[0]) * 365
                 + (end[1] - start[1]) * 30
                 + (end[2] - start[2]) + 1

    options.rule = false
    if(user.role == '1'){
      if (options.t_version == '0')
        options.rule = true
    } 
    if (user.role == '2') {
      if (options.l_version == '0')
        options.rule = true
    } 

    this.setData({detail: options})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})