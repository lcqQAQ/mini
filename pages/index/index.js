//index.js
//获取应用实例
const app = getApp()
Page({
  data: {
    userInfo:{},
    user:{}
  },
  //事件处理函数
  leave: function(){
   const url = this.data.user.role === 0 ? 
        '../leave/zero/zero' : '../leave/leave'

   wx.navigateTo({
     url: url,
   })
  },
  cet: () => {
    wx.navigateTo({
      url: '../cet/cet',
    })
  },
  intro: () => {
    wx.navigateTo({
      url: '../introduce/introduce',
    })
  },
  onLoad: function () {
    const user = wx.getStorageSync('user') || {}

    this.setData({ user: user })
  },
  onShow: function () {
    app.getUserInfo((userInfo) => {
      this.setData({ userInfo: userInfo })

      if (!this.data.user.studentId) {
        wx.showModal({
          title: '友情提示',
          content: '很抱歉未能自动获取到您的学工号,请手动登录本系统',
          showCancel: false,
          confirmText: '手动登录',
          success: (res) => {
            wx.navigateTo({
              url: '../id/id',
            })
          }
        })
      }
      
    })
  },
  onPullDownRefresh: function () {
    setTimeout(
      ()=>{wx.stopPullDownRefresh()},1200)
  },
  onShareAppMessage: () => {
    console.log('share')
  }
})
