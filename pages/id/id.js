import { login } from '../../assest/api.js'
// id.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  submit: ({detail}) => {
    
    let value = detail.value
    let id_length = value.student_id.length
    if (id_length === 13 || id_length === 8 || id_length === 9){
      wx.showLoading({
        title: '请稍等...',
        mask: true
      })

      login(value, (res) => {
        // wx.hideLoading()
        const data = res.data
        if(data.code != 200){
          wx.showToast({
            title: data.result || '网络阻塞，请稍后再试',
            icon: 'image',
            image: '../../assest/img/error.png',
            duration: 2500,
            mask: true
          })
        } else {
          wx.setStorageSync('user', data.result)
          wx.reLaunch({
            url: '../index/index',
          })
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '请正确输入13位学号',
        showCancel: false,
        confirmText: '好'
      })
    }
  }
})